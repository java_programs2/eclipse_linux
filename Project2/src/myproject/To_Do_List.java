package myproject;

import java.sql.*;
import java.util.Scanner;

public class To_Do_List {

	 
	public static void main(String[] args) throws Exception {
		
		activity();
}
	
	public static void activity() throws Exception {

		Scanner sc=new Scanner(System.in);
			System.out.println("press 1 to display");
			System.out.println("press 2 to add");
			System.out.println("press 3 to update");
			System.out.println("press 4 to remove");
			int a=sc.nextInt();
			
			if(a==1) {
				readRecords();
			}
			if(a==2) {
				addRecords();
			}
			if(a==3) {
				updateRecords();
			}
			if(a==4) {
				remove();
			}
		
	}
	
	
public static void readRecords() throws Exception{
	
	String url="jdbc:mysql://localhost:3306/myproject";
	String username="praveen";
	String password="example";
	String query="select * from todolist";
	Connection con=DriverManager.getConnection(url,username,password);
	Statement st=con.createStatement();
	ResultSet rs=st.executeQuery(query);
	
	while(rs.next()) 
	{
		System.out.print(rs.getInt(1)+"."+rs.getString(2));
		System.out.println();
	}
	con.close();
	System.out.println();
	
	activity();
}


public static void addRecords() throws Exception{
	
	try {
		String url="jdbc:mysql://localhost:3306/myproject";
		String username="praveen";
		String password="example";
		
			Scanner obj=new Scanner(System.in);
			System.out.println("Enter no and activity");
			int no=obj.nextInt();
		
		Scanner obj2=new Scanner(System.in);
		String activity=obj2.nextLine();
		
		String query="insert into todolist values(?,?)";
		Connection con=DriverManager.getConnection(url,username,password);
		PreparedStatement pst=con.prepareStatement(query);
		pst.setInt(1, no);
		pst.setString(2, activity);
		int rows=pst.executeUpdate();
			
		System.out.println("Successfully added "+rows+ " activity");
		con.close();
		System.out.println();
			
		activity();
	}
	catch(Exception e) {
		System.out.println("invalid input");
		activity();
	}
}


public static void remove() throws Exception{
	
	String url="jdbc:mysql://localhost:3306/myproject";
	String username="praveen";
	String password="example";
	
	System.out.println("Enter activity");
	Scanner obj=new Scanner(System.in);
	int no=obj.nextInt();
	
	String query="delete from todolist where no=?";
	Connection con=DriverManager.getConnection(url,username,password);
	PreparedStatement pst=con.prepareStatement(query);
	pst.setInt(1, no);
	int rows=pst.executeUpdate();
	System.out.println("Successfully deleted "+rows+ " activity");
	con.close();
	System.out.println();
	
	activity();
}

public static void updateRecords() throws Exception{
	
	String url="jdbc:mysql://localhost:3306/myproject";
	String username="praveen";
	String password="example";
	
	System.out.println("Enter no and activity");
	Scanner obj=new Scanner(System.in);
	int no=obj.nextInt();
	Scanner obj2=new Scanner(System.in);
	String activity=obj2.nextLine();
	
	String query="update todolist set activity=? where no=?";
	Connection con=DriverManager.getConnection(url,username,password);
	PreparedStatement pst=con.prepareStatement(query);
	pst.setString(1, activity);
	pst.setInt(2, no);
	int rows=pst.executeUpdate();
	
	System.out.println("Successfully updated "+rows+ " activity");
	con.close();
	System.out.println();
	
	activity();
}

}
