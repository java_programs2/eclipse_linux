package set_learning;

import java.util.Iterator;
import java.util.NavigableSet;
import java.util.TreeSet;

public class Treeset_1 {

	public static void main(String[] args) {
		
		TreeSet ts=new TreeSet();
		ts.add(2);
		ts.add(5);
		ts.add(6);
		ts.add(15);
		System.out.println(ts);
		System.out.println(ts.ceiling(28));
		System.out.println(ts.comparator());
		Iterator it=ts.descendingIterator();
		while(it.hasNext()) {
			System.out.print(it.next()+" ");
		}
		System.out.println();
		
		TreeSet ts2=new TreeSet();
		ts2.add("praveen");
		ts2.add("kumar");
		ts2.add("anand");
		ts2.add("vignesh");
		
System.out.println(ts2);
Iterator it2=ts2.descendingIterator();
while(it2.hasNext()) {
	System.out.print(it2.next()+" ");
}
System.out.println();
NavigableSet nv=ts2.descendingSet();
Iterator it3=nv.iterator();
while(it3.hasNext()) {
	System.out.print(it3.next()+" ");
}
	}

}
