package map_learning;


import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

public class Map_demo {

	public static void main(String[] args) {
		
		HashMap hm=new HashMap();
		
		hm.put("idli", 40);
		hm.put("poori", 30);
		hm.put("dosa", 40);
		hm.put("pongal", 50);
		
		System.out.println(hm);
		System.out.println(hm.containsKey("idli"));
		System.out.println(hm.containsValue(30));
		System.out.println(hm.entrySet());
		System.out.println(hm.keySet());
		System.out.println(hm.values());
		
		Set s=hm.entrySet();
		
		for(Object ob:s) {
			Entry e=(Entry)ob;
			int ss=(Integer)e.getValue();
			if(ss==30) {
				System.out.println(e.getKey());
			}
		}
		
		}

}
