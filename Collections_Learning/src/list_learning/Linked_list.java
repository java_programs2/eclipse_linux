package list_learning;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

public class Linked_list {

	public static void main(String[] args) {
		
		ArrayList<Integer>al=new ArrayList<Integer>();
		LinkedList<Integer>ll=new LinkedList<Integer>();
		LinkedList<Integer>ll2=new LinkedList<Integer>();
		al.add(10);
		al.add(8);
		al.add(15);
		
		//1.add
		ll.add(10);
		ll.add(8);
		ll.add(15);
		
		ll.add(45);
		ll.add(68);
		ll.add(23);
		System.out.println(al);
		System.out.println(ll);
		
		ll.add(0, 4);
		ll.addAll(ll2);
		System.out.println(ll);
		ll.addAll(1, ll2);
		System.out.println(ll);
		
		//2. addfirst
		ll.addFirst(1);
		System.out.println(ll);
		
		//3.addlast()
		ll.addLast(37);
		System.out.println(ll);
		
		//4. void clear
//		ll.clear();
//		System.out.println(ll);
		
		//4. clone
		Object o=ll.clone();
		System.out.println(o);
		
		//5. contains
		
		System.out.println(ll.contains((Object)37));
		
		//6. descendingiterator
		Iterator a=ll.descendingIterator();
		
		while(a.hasNext()) {
			System.out.print(a.next()+" ");
		}
		System.out.println();
		//7.element --  to fetch 1st elements from list
		int q=ll.element();
		System.out.println(q);
		
		//8. get
		int q1=ll.get(3);
		System.out.println(q1);
		
		//9.getfirst
		System.out.println(ll.getFirst()); 
		
		//10 ListIterator
		
		ListIterator liit=ll.listIterator(3);
		
		while(liit.hasNext()) {
			System.out.print(liit.next()+" ");
		}
		System.out.println();
		
		//11 offer
		ll.offer(43);
		System.out.println("offer "+ll);
		ll.add(22);
		System.out.println(ll);
		
		//12 offerfirst
		ll.offerFirst(18);
		System.out.println(ll);
		
		//13.offerlast
		ll.offerLast(11);
		System.out.println(ll);
		
		
		//14.peek
		System.out.println(ll.peek());
		
		//15 peeklast
		System.out.println(ll.peekLast());
		
		//16 poll
		System.out.println(ll.poll());
		System.out.println(ll);
		
		//17 remove()
		System.out.println(ll.remove());
		System.out.println(ll);
		
		//18 remove
		System.out.println(ll.remove(2));
		System.out.println(ll);
		
		//19 remove first occurance
		ll.add(15);
		System.out.println(ll);
		System.out.println(ll.removeFirstOccurrence(15));
		System.out.println(ll);
		
		//20. toarray
		Integer[]c=new Integer[ll.size()];
		c=ll.toArray(c);
		for(int i=0; i<c.length; i++)
		{
			System.out.print(c[i]+" ");
		}
		System.out.println();
		
	}
}
