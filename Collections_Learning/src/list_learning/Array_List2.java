package list_learning;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Array_List2 {

	public static void main(String[] args) {
		Array_List2 a=new Array_List2();
		ArrayList al=new ArrayList();
		al.add(13);
		al.add(10);
		al.add(20);
		al.add(5);
		al.add(30);
		al.add(40);
		al.add(1);
		
		
		System.out.println(al.contains(20));
		
		ArrayList al2=new ArrayList();
		al2.add(10);
		al2.add(20);
		al2.add(30);
		
		
System.out.println(al.containsAll(al2));


//al.add(a);
System.out.println(al);

Object ob=al.get(0);
System.out.println(ob);
Integer i=(Integer)(ob);
System.out.println(i+10);

int b=al.size();
for(int j=0; j<b; j++) {
	System.out.println(al.get(j));
}
System.out.println();



for(int j=0; j<al.size(); j++) {
	Object obj=al.get(j);
	Integer c=(Integer)(obj);
	
	if(c%2!=0) {
		System.out.println(c);
	}
}
System.out.println();
System.out.println();

ArrayList al3=new ArrayList();
al3.add("abcd");
al3.add("bacde");
al3.add("fjddf");
al3.add("afkalsf");

for(int k=0; k<al3.size(); k++) {
	
	Object obj2=al3.get(k);
	String a1=(String)(obj2);
	
	if (a1.charAt(0)=='a') {
		System.out.println(a1);
	}
	
}


al.add(20);
System.out.println(al);
System.out.println(al.indexOf(20)); // forward direction
System.out.println(al.lastIndexOf(20)); // reverse direction
System.out.println(al.isEmpty());
System.out.println(al.remove(3));  // index
System.out.println(al);
al.add("abc");
System.out.println(al);
System.out.println(al.remove((Object)10)); // remove by object
System.out.println(al);


ArrayList al4=new ArrayList();

al4.add(30);
al4.add("abc");
//al.removeAll(al4);
//System.out.println(al);

//al.retainAll(al4);
//System.out.println(al);

al.set(0, 45);
System.out.println(al);
Object[]str=al.toArray();
for(Object o:str) {
	System.out.println(o);
}

al.add(43);
al.add("ddfa");
al.add(false);

System.out.println(al);

List l=al.subList(1, 5);

System.out.println(l);


ArrayList all=new ArrayList();
all.add(10);
all.add(20);
all.add(15);
all.add(5);
Collections.sort(all);
System.out.println(all);
	}

}
