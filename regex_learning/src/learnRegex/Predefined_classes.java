package learnRegex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Predefined_classes {

	public static void main(String[] args) {
		String sentence="Tamil Tamilan Tamilnadu 60028 !@#%$";
		//Pattern p=Pattern.compile("\\w");   // words except special character
		//Pattern p=Pattern.compile("\\W"); // only special character
		//Pattern p=Pattern.compile("\\bTamil");
		//(".")- to print all in string 
		Pattern p=Pattern.compile("\\bTamil\\b");
		Matcher m=p.matcher(sentence);
		
		while(m.find()) {
			System.out.print(m.group() );
		}

	}

}
