package learnRegex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Quantifiers_demo {

	//quantifiers
	
	
	public static void main(String[] args) {
		String s="ababbaaabbbabab";
		//Pattern p= Pattern.compile("a+"); 
		//Pattern p= Pattern.compile("a*");// 
		//Pattern p= Pattern.compile("a?");
		Pattern p= Pattern.compile("a{3}");
		Matcher m=p.matcher(s);
		while(m.find()) {
			//System.out.println(m.group() + " starts at " +m.start());
			System.out.println(m.group() + " starts at " +m.start());
		}

	}

}
