package learnRegex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex_demo {

	public static void main(String[] args) {
		
//		String input="My mobile no is 9486251451";
//		Pattern pobj= Pattern.compile("\\d{10}");  // digits
//		Pattern pobj= Pattern.compile("[0123456789]");
//		Pattern pobj= Pattern.compile("[0-9]");
//		Pattern pobj= Pattern.compile("[a-z]");
//		Matcher mobj=pobj.matcher(input);
//		
//		while(mobj.find()) {
//			System.out.print(mobj.group());
//			System.out.println(mobj.start());
//			System.out.println(mobj.end());
//		}
		
//		
//		String password="Chennai is capital of tamilnadu";
//		//Pattern pobj= Pattern.compile("tamilnadu$");  // $ ends with
//		Pattern pobj= Pattern.compile("^Chennai"); 
//		Matcher mobj=pobj.matcher(password);
//		
//		while(mobj.find()) {
//			System.out.print(mobj.group());
//
//		}
//		
//		
//		String password="Chennai123@gmail.com";
//		//Pattern pobj= Pattern.compile("tamilnadu$");  // $ ends with ^starts with
//		Pattern pobj= Pattern.compile("[^A-Za-z0-9]");  // only symbols
		//Pattern pobj= Pattern.compile("[A-Za-z0-9]"); // except symbols
//		Matcher mobj=pobj.matcher(password);
//		
//		while(mobj.find()) {
//			System.out.print(mobj.group());
//
//		}
//		
		
//		String password="Chennai is capital of tamilnadu";
//		
//		//Pattern pobj= Pattern.compile("\\s"); // \\s for space
//		Pattern pobj= Pattern.compile("\\S"); // \\S for ignoring space
//		Matcher mobj=pobj.matcher(password);
//		int c=0;
//		while(mobj.find()) {
//			c++;
//			System.out.print(mobj.group());
//			
//		}	
//		System.out.println(c);
//		
		
//String a="Chennai 600042 is velachery";
//		
//		
//		Pattern pobj= Pattern.compile("\\D"); // \\S for ignoring digit
//		Matcher mobj=pobj.matcher(a);
//		
//		while(mobj.find()) {
//			
//			System.out.print(mobj.group());
//			
//		}	
		
//	String b="3486251451";
//	Pattern pobj= Pattern.compile("[6-9][0-9]{9}"); 
//	Matcher mobj=pobj.matcher(b);
//	int k=0;
//	while(mobj.find()) {
//		k=1;
//		System.out.print(mobj.group());
//	}
//	if(k==0) {
//		System.out.println("not a mobile no");
//	}
	
		
//		String mob="999486251451";
//		Pattern pobj= Pattern.compile("(0|91)?[6-9][0-9]{9}"); 
//		Matcher mobj=pobj.matcher(mob);
//		int k=0;
//		while(mobj.find()) {
//			k=1;
//			System.out.print(mobj.group());
//		}
//		if(k==0) {
//			System.out.println("not a mobile no");
//		}
		
//		String pattern="-";
//		String input="28-March-2023";
//		Pattern pobj=Pattern.compile(pattern);
//		String[]items=pobj.split(input);
//		for(int i=0; i<items.length;i++) {
//			System.out.println(items[i]);
//		}
		
		String pattern="fa";
		String input="fafafafaffaaffaafafafa";
		Pattern paobj=Pattern.compile(pattern);
		Matcher mobj=paobj.matcher(input);
		while(mobj.find()) {
			System.out.print(mobj.group()+" ");
		}
	}

}
