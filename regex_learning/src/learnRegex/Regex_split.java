package learnRegex;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex_split {

	public static void main(String[] args) {
		String s1="www.praveen.com";
		Pattern p= Pattern.compile("\\.");
		String s2[]=p.split(s1);
		System.out.println();
		System.out.println(Arrays.toString(s2));
	}

}
