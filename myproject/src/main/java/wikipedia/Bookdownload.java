package wikipedia;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class Bookdownload {
	

	
	    public static void main(String[] args) throws InterruptedException {
	    	 // Set the path to your ChromeDriver
	        System.setProperty("webdriver.chrome.driver", "/home/praveenkumar/Documents/chrome driver/chromedriver-linux64/chromedriver");

	        // Initialize the ChromeOptions
	        ChromeOptions options = new ChromeOptions();
	        
	        // Configure to accept insecure certificates
	        options.setAcceptInsecureCerts(true);
	        
	        // Initialize the ChromeDriver
	        WebDriver driver = new ChromeDriver(options);

	        // Open the target URL
	        driver.get("https://www.tamildigitallibrary.in/tva-search?tag=%E0%AE%95%E0%AF%8A%E0%AE%A4%E0%AF%88%E0%AE%A8%E0%AE%BE%E0%AE%AF%E0%AE%95%E0%AE%BF");

	        // Find the elements containing the book links
	        // You need to adjust the selector based on the website structure
	        List<WebElement> bookLinks = driver.findElements(By.cssSelector("css_selector_of_book_links"));

	        // Iterate through each book link
	        for (WebElement bookLink : bookLinks) {
	            // Click on the book link
	            bookLink.click();

	            // Wait for the page to load if necessary
	            try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}  // Adjust sleep time as needed

	            // Find the download button
	            // You need to adjust the selector based on the website structure
	            WebElement downloadButton = driver.findElement(By.cssSelector("css_selector_of_download_button"));

	            // Click the download button
	            downloadButton.click();

	            // Wait for the download to complete
	            Thread.sleep(5000);  // Adjust sleep time as needed

	            // Go back to the main page
	            driver.navigate().back();

	            // Wait for the page to load if necessary
	            Thread.sleep(2000);  // Adjust sleep time as needed
	        }

	        // Close the browser
	        driver.quit();
	           
	    }
	}

 




