package wikipedia;



import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
public class Read_LInk {

	public static void main(String[] args) {
		

		

	
		        // Path to your Excel file
		        String excelFilePath = "/home/praveenkumar/Documents/Untitled 1.xlsx";

		        try {
		            FileInputStream excelFile = new FileInputStream(new File(excelFilePath));
		            XSSFWorkbook workbook = new XSSFWorkbook(excelFile);

		            // Get the first sheet from the workbook
		            XSSFSheet sheet = workbook.getSheetAt(0);

		            // Iterate through all rows in the sheet
		            Iterator<Row> rowIterator = sheet.iterator();
		            while (rowIterator.hasNext()) {
		                Row row = rowIterator.next();

		                // Iterate through all cells in the row
		                Iterator<Cell> cellIterator = row.cellIterator();
		                while (cellIterator.hasNext()) {
		                    Cell cell = cellIterator.next();

		                    // Check if the cell contains a hyperlink
		                    if (cell.getHyperlink() != null) {
		                        // Get the hyperlink address
		                        String linkAddress = cell.getHyperlink().getAddress();

		                        // Open the link in the default browser
		                        openLinkInBrowser(linkAddress);
		                    }
		                }
		            }

		            // Close the workbook and file input stream
		            workbook.close();
		            excelFile.close();
		        } 
		        catch (Exception ex) {
		            ex.printStackTrace();
		        }
		    }

		    // Method to open a URL in the default browser
		    private static void openLinkInBrowser(String link) {
		        try {
		            Desktop.getDesktop().browse(new URI(link));
		        } catch (IOException | java.net.URISyntaxException e) {
		            e.printStackTrace();
		        }
		    }
		}


