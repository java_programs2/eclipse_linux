package string_programs;

import java.util.Scanner;

public class Consonants_vowels {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter a word");
		String name=sc.next();
		int len=name.length();
		int vow=0;
		int con=0;
		for(int i=0; i<len; i++)
		{
			char ch=name.charAt(i);
			
			if(ch=='a' || ch=='e' || ch=='i' || ch=='o' || ch=='u' || ch=='A' || ch=='E' || ch=='I' || ch=='O' || ch=='U')
					{
					System.out.print(ch+" ");
					vow++;
			}
			else {
				con++;
			}
		}
		System.out.println();
		System.out.println("count of vowels :"+vow);
		System.out.println("count of consonants :"+con);
	}

}
