package payilagam;

public class Exception_demo {

	public static void main(String[] args) {
		
		int n1=10;
		int n2=2;
		int a[]= {1,2,3,4};
		String name="praveen";
		
		try {
			System.out.println(n1/n2);
			System.out.println(a[3]);
			System.out.println(name.charAt(1));
			int nums[]=new int[-5];
			System.out.println(nums.length);
		}
			
		catch(ArithmeticException obj) {
			System.out.println("invalid input");
		}
		catch(ArrayIndexOutOfBoundsException arr) {
			System.out.println("array index value is out of range");
		}
	catch(StringIndexOutOfBoundsException q) {
		System.out.println("string index value is out of range");
		q.printStackTrace();
	}
catch(Exception e) {
	
}

finally {
	System.out.println("This is final block");
}
}

}
