package string_class;

public class Ends_with_demo {

	public static void main(String[] args) {
		String n1="praveenkumar";
		String n2="kumar";
		System.out.println(Find_ends_with(n1,n2));

	}

	public static boolean Find_ends_with(String n1, String n2) {
		
		int j=n1.length()-1;
		for(int i=n2.length()-1; i>=0;i--)
		{
			if(n2.charAt(i)==n1.charAt(j--))
			{
				continue;
			}
			else 
			{
				return false;
			}
		}
	
		return true;
	}

}
