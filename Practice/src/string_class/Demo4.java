package string_class;

import java.util.Arrays;

public class Demo4 {

	public static void main(String[] args) {
		
String word1="abe";
String word2="abc";
//1.compareTo
System.out.println(word1.compareTo(word2));    

//2.compareToIgnoreCase
String a="Abc";
String b="abc";
System.out.println(a.compareToIgnoreCase(b));


//3.concat
String n1="praveen";
String n2="kumar";

System.out.println(n1.concat(n2));

//4. ends with
String a1="praveenkumar";
String a2="kumar";
System.out.println(a1.endsWith(a2));


//5. starts with

String b1="praveenkumar";
String b2="praveen";
System.out.println(b1.startsWith(b2));

//6.equals
String c="hello";
String d="hello";
System.out.println(c.equals(d));

//7.equalsignorecase

String c1=" hello ";
String d1="Hello";
System.out.println(c1.equalsIgnoreCase(d1));

// 8 isEmpty
System.out.println(c1.isEmpty());

// 9 indexof
System.out.println(c1.indexOf('l'));

//10 lastindexof
System.out.println(c1.lastIndexOf('l'));

//11 substring
System.out.println(c1.substring(1));
System.out.println(c1.substring(1, 4));

//12 tochararray
System.out.println(Arrays.toString(c1.toCharArray()));

//13 to lowercase
System.out.println(d1);

//14 to uppercase
System.out.println(c1);

//14 trim
System.out.println(c1.trim());

//15 split
String q="hello praveen kumar";
System.out.println(Arrays.toString(q.split(" ")));

}

}
