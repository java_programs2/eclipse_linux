package javaconcepts;

public class Trainer {

	String dept;
	String institute;
	private int salary=10000;
	
	Trainer(String dept,String institute){
		this.dept=dept;
		this.institute=institute;
		}
	
public static void main(String[] args) {
		Trainer trainerKumar=new Trainer("CSE","payilagam");

		trainerKumar.training();

	}
	public void training() {
		
		System.out.println(dept+" "+institute);
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}

}
