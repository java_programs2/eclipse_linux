package javaconcepts;

public class Arraymin {

	public static void main(String[] args) {
		
		int a[]= {10,20,30,40,50,60,70,80,90,100};
		Arraymin obj=new Arraymin();
		System.out.println(obj.small_elements(a));
		System.out.println(obj.big_elements(a));
	}
	
	public int small_elements(int[] a) {
		int smallno=a[0];
		for(int i=0;i<a.length;i++) {
			if(a[i]<smallno) {
				smallno=a[i];
			}
		}
		
		return smallno;
	}
	
	public int big_elements(int[] a) {
		int bigno=a[0];
		for(int j=0;j<a.length;j++) {
			if(a[j]>bigno) {
				bigno=a[j];
			}
		}
		
		return bigno;
	}	
}
