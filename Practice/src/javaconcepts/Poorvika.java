package javaconcepts;

public class Poorvika extends Poorvika_HO{

	int price;
	int discount;
	
	public Poorvika() {
		System.out.println("welcome");
	}
	
	public Poorvika(int price) {
		this.price=price;
		System.out.println(price);
		System.out.println(super.specialdiscount);
	}
	
	public Poorvika(int price, int discount) {
		this.discount=(price/100)*discount;
		this.price=price-this.discount;
	}
	
	public int buy() {
		return price;
	}
	public static void main(String[] args) {
	
		Poorvika welcome= new Poorvika();
		Poorvika redmi=new Poorvika(12000);
		Poorvika oppo=new Poorvika(14000);
		Poorvika vivo=new Poorvika(10000);
		Poorvika apple= new Poorvika(50000,10);
		
		System.out.println(redmi.buy());
		System.out.println(apple.buy());
	}

}
