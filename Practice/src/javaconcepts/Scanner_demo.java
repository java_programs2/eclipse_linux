package javaconcepts;

import java.util.Scanner;

public class Scanner_demo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc=new Scanner(System.in);
		/*System.out.println("Enter the number");
		
		int num=sc.nextInt();
		for(int count=1;count<=num;count++)
		{
			System.out.println(count+" "); 
		} */
		
		System.out.println("Enter the name");
		String name=sc.nextLine(); // it prints the word after space
		//String name=sc.next();  //it ignore the word after the space
		System.out.println("Hi "+name+" welcome to java");
	}

}
