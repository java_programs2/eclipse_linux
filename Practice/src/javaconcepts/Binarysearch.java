package javaconcepts;

import java.util.Scanner;

public class Binarysearch {

	
	public static void main(String[] args) {
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter the array size");
		int size=sc.nextInt();
		int arr[]=new int[size];
		System.out.println("Enter the values");
		for(int i=0;i<arr.length;i++) {
			arr[i]=sc.nextInt();
			}
		System.out.println("Enter the target");
		int target=sc.nextInt();
		int start=0;
		int end=arr.length-1;
		boolean condition=false;
		
		while(start<=end) {
			int mid=(start+end)/2;
			if(arr[mid]==target)
			{
				System.out.println(arr[mid]+" "+"is founded");
				condition =true;
			}
			if(arr[mid]<target) {
				start=mid+1;
			}
			else
			{
				end=mid-1;
			}
		}
		if(condition==false) {
			System.out.println("Target not found");
		}
	}

}
