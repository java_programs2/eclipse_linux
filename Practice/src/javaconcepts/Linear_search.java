package javaconcepts;

public class Linear_search {
	       
	static int target=100;
	
	
	public static void main(String[] args) {
		int a[]= {10,20,30,40,50,60}; 
		Linear_search obj=new Linear_search();
		System.out.println("Index of "+target+" is"+" "+obj.find_elements(a));
	}

	public int find_elements(int[]a) 
	{
		
		for(int i=0; i<a.length; i++) {
			if(a[i]==target)
			{
				
				return i;
			
			}
			
		}
		
		return -1;
	}

}
