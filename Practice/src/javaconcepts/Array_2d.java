package javaconcepts;

import java.util.Scanner;

public class Array_2d {

	public static int[][] Create_Array(){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter row value");
		int row_size=sc.nextInt();
		System.out.println("Enter col value");
		int col_size=sc.nextInt();

		int arr[][]=new int[row_size][col_size];
		System.out.println("Enter "+row_size*col_size+" numbers ");

		for(int row=0; row<row_size; row++)
		{
			for(int col=0; col<col_size; col++)
			{
				arr[row][col]=sc.nextInt();
			}
		}
		return arr;
	}
	
	public static void main(String[] args) {
		Array_2d obj=new Array_2d();
		System.out.println("Array1");
		int num1[][]=Create_Array();
		System.out.println("Array2");
		int num2[][]=Create_Array();
		
		for(int row=0; row<num1.length; row++)
		{
			for(int col=0; col<num1.length; col++)
			{
				System.out.print(num1[row][col]+num2[row][col]+" ");	
			}
			System.out.println();
		}
	}

}
