package javaconcepts;

import java.util.Arrays;

public class Selection_sort {

static int find_min_number(int arr[], int start_index) {
	
int small_number=start_index;
for(int i=start_index+1; i<arr.length; i++) {
	if(arr[i]<arr[small_number])
		small_number=i;
}
	return small_number;
}
static int[] selectionSort(int arr[], int n)
{
	for(int i=0; i<n; i++) {
		int small_index=find_min_number(arr,i);
		
		int temp=arr[i];
		arr[i]=arr[small_index];
		arr[small_index]=temp;
	}
	
	return arr;
}
	public static void main(String[] args) {
		
		int arr[]= {4,1,3,9,7};
		
		selectionSort(arr, arr.length);
		System.out.println(Arrays.toString(arr));
		
		
	}

}
