package javaconcepts;

import java.util.Scanner;

public class Loopeg1 {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the no");
		int num=sc.nextInt();
		
		for(int row=num;row>=1;row--)
		{
			for(int col=1;col<row;col++)
			{
				System.out.print(col+" ");
			}
			System.out.println();
		}
	}

}
