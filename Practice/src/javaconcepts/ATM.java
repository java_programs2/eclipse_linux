package javaconcepts;

public class ATM extends Bank{

	

	@Override
	public void withdraw() {
		
		System.out.println("you can withdraw in ATM");
	}

	@Override
	public void printStatement() {
		System.out.println("you can printstatement in ATM");
		
	}

	public static void main(String[] args) {
		
		
		ATM iob=new ATM();
		
		iob.deposit();
		iob.withdraw();
		iob.printStatement();
	}
	
}
