package javaconcepts;

public class Two_D_Array {

	public static void main(String[] args) {
		int arr[][]= {{10,20,30},
					  {40,50,60},
					  {70,80,90}};
		int target=80;
		System.out.println(search_2d_array(arr,target));
		Add_Diagonal(arr,target);

	}

	public static void Add_Diagonal(int[][] arr, int target) {
		int sum=0;
		for(int row=0; row<arr.length; row++)
		{
			
			for(int col=0; col<arr[row].length; col++)
			{
				if(row==col || row+col==arr.length-1 || (arr.length)/2==col || (arr.length)/2==row)
				{
					sum=sum+arr[row][col];
				}
			}
		}
		
		System.out.println(sum);
	}
	
	
	

	public static boolean search_2d_array(int[][] arr, int target) {
		
		for(int row=0; row<arr.length; row++)
		{
			for(int col=0; col<arr[row].length; col++)
			{
				if(arr[row][col]==target)
				{
					return true;
				}
			}
		}
		
		return false;
		}

}
