package javaconcepts;

public class Array_sorting {

	public static void main(String[] args) {
		int a[]= {3,1,6,2,5,4};
		
		for(int i=0; i<a.length; i++)
		{
			for(int j=i+1; j<a.length; j++)
			{
				if(a[i]<a[j])
				{
					int temp=a[i];
					a[i]=a[j];
					a[j]=temp;
				}
			}
		}
		for(int i=0; i<a.length; i++) {
			System.out.print(a[i]+" ");
		}

	}

}
