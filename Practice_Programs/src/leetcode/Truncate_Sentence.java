package leetcode;

import java.util.Arrays;

public class Truncate_Sentence {

	public static void main(String[] args) {
		
		String s = "Hello how are you Contestant";
		int k = 4;
		
		String[]a=s.split(" ");
		System.out.println(Arrays.toString(a));
		
		String b="";
		for(int i=0; i<k; i++) {
			b+=a[i]+" ";
		}
		
		System.out.println(b.trim());
	}

}
