package leetcode;

import java.util.Arrays;

public class String_Reverse {

	public static void main(String[] args) {
		String s="  hello world  ";
		System.out.println(reverseWords(s));
	}
	 public static String reverseWords(String s) {
	 System.out.println(s.trim());
		 String []str=s.trim().split("\\s+");
		 System.out.println(Arrays.toString(str));
		 String out="";
		 for(int i=str.length-1; i>0; i--) {
			 out+=str[i]+" ";
		 }
		 return out+str[0];
	 }
}
