package leetcode;

import java.util.Arrays;

public class Sum_of_digit {

	public static void main(String[] args) {
		int nums[] = {1,2,3,4};
		int[]a=new int[nums.length];
		
		a[0]=nums[0];
		
		int index=1;
		for(int i=0; i<nums.length; i++) {
			for(int j=0; j<=i+1; j++) {
				a[index++]=nums[i]+nums[j];
			}
		}
		System.out.println(Arrays.toString(a));
		
	}

}
