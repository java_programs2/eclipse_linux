package leetcode;

import java.util.Arrays;

public class Common_divisor {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int []nums = {2,5,6,9,10};
		Arrays.sort(nums);
		int first=nums[0];
		int last=nums[nums.length-1];
		int max=0;
		for(int i=1; i<=last; i++) {
			if(first%i==0 && last%i==0) {
				if(max<i) {
					max=i;
				}
			}
		}
		System.out.println(max);
	}

}
