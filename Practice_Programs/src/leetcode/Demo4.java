package leetcode;

import java.util.Arrays;

public class Demo4 {

	public static void main(String[] args) {
		int nums[]= {5,7,7,8,8,10};
		int target=8;
		
		int a[]=(searchRange(nums,target));
		System.out.println(Arrays.toString(a));
	}
	 public static int[] searchRange(int[] nums, int target) {
		 
		 int b[]= {-1,-1};
		 int count=0;
	     for(int i=0; i<nums.length;i++) {
	    	 if(target==nums[i]) {
	    		 count++;
	    	 }
	     }
	     if(count==0) {
	    	 return b;
	     }
	      int a[]=new int[count];
	      int j=0;
	      for(int i=0; i<nums.length;i++) {
		    	 if(target==nums[i]) {
		    		 a[j++]=i;
		    	 }
		     }
	      Arrays.toString(a);
	      int result[]=new int[2];
	      result[0]=a[0];
	      result[1]=a[a.length-1];
		 return result;
	    }
}
