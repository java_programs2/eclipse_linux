package leetcode;

import java.util.Arrays;

public class Third_Largest_no {

	
	public static void main(String[] args) {
		int[] nums = {1,2,2,5,3,5};
		System.out.println(Third_largest(nums));
		
		
	}

	private static int Third_largest(int []nums) {
		
		Arrays.sort(nums);
		int l=nums.length;
		
		if(l==1) {
			return (nums[0]);
		}
		
		if(l==2) {
			if(nums[0]<nums[1]) {
				return nums[1];
			}
			else
				return nums[0];
		}
		
		if(l==3) {
			if(nums[0]<nums[1] && nums[1]<nums[2]) {
				return nums[0];
			}
			else if(nums[0]==nums[1] && nums[1]<nums[2]) {
				return nums[2];
			}
			else if(nums[0]<nums[1] && nums[1]==nums[2]) {
				return nums[2];
			}
			
			else
				return nums[2];
		}
		
		
		
		if(l==4) {
			if(nums[0]<nums[1] && nums[1]<nums[2] && nums[2]<nums[3]) {
				return nums[1];
			}
			else if(nums[2]<nums[3] && nums[1]<nums[2]) {
				return nums[1];
			}
			else if(nums[0]<nums[1] && nums[1]==nums[2] && nums[2]<nums[3]) {
				return nums[0];
			}
			
			else if(nums[0]==nums[1] && nums[1]==nums[2] && nums[2]<nums[3]) {
				return nums[3];
		}
		else if(nums[0]<nums[1] && nums[1]<nums[2] && nums[2]==nums[3]) {
			return nums[0];
		}
		else if(nums[0]<nums[1] && nums[1]==nums[2] && nums[2]==nums[3]) {
			return nums[3];
		}
		else
			return nums[3];
	}
		
		if(l>4) {
			if(nums[l-3]<nums[l-2] && nums[l-2]<nums[l-1]) {
				return nums[l-3];
			}
			if(nums[l-4]<nums[l-3] && nums[l-3]<nums[l-2] && nums[l-2]==nums[l-1]) {
				return nums[l-4];
			}
		}
		return 1;
	}
}
