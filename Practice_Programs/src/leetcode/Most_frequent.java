package leetcode;

import java.util.Arrays;

public class Most_frequent {

	public static void main(String[] args) {
		int []nums = {0,1,2,2,4,4,1};
		int count=0;
		for(int i=0; i<nums.length; i++) {
			if(nums[i]>0 && nums[i]%2==0) {
				count++;
			}
		}
		int a[]=new int[count];
		int index=0;
		
		for(int i=0; i<nums.length; i++) {
			if(nums[i]>0 && nums[i]%2==0) {
				a[index++]=nums[i];
			}
		}
		System.out.println(Arrays.toString(a));
		
		
	}

}
