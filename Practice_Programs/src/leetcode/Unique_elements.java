package leetcode;

import java.util.Arrays;

public class Unique_elements {

	public static void main(String[] args) {
		int []nums = {1,1,1,1};
		
		
				int[] freq = new int[nums.length];

		for (int i = 0; i < nums.length; i++) {
			
			int count = 1;
			for (int j = i + 1; j < nums.length; j++) {

				if (nums[i] == nums[j]) {
					count++;
					freq[j] = -1;
				}
			}
			if (freq[i] != -1) {
				freq[i] = count;
			}
		}
		System.out.println(Arrays.toString(freq));
		int s=0;
		int a=0;
		for (int i = 0; i < nums.length; i++) {
			if (freq[i] == 1) {
				s++;
				a+=nums[i];
			}
		}
		System.out.println(a);
	}

}
