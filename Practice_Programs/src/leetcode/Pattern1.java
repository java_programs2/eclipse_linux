package leetcode;

public class Pattern1 {

	public static void main(String[] args) {
		
		for(int r=1; r<=3; r++) {
			for(int s=3; s>=r; s--) {
				System.out.print(" ");
			}
			for(int c=1; c<=r; c++) {
				System.out.print(" *");
			}
			for(int c2=1; c2<r; c2++) {
				System.out.print(" *");
			}
			System.out.println();
		}
	}

}
