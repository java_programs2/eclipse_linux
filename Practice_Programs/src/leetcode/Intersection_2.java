package leetcode;

import java.util.Arrays;

public class Intersection_2 {

	public static void main(String[] args) {
		int []nums1 = {4,9,5};
		int []nums2 = {9,4,9,8,4};
Arrays.sort(nums1);
Arrays.sort(nums2);
int l1=nums1.length;
int l2=nums2.length;

int count=0;

for(int i=0; i<l1; i++) {
	int a=nums1[i];
	for(int j=0; j<l2; j++) {
		if(a==nums2[j]) {
			count++;
		}
	}
}

int b[]=new int[count];
int index=0;
for(int i=0; i<l1; i++) {
	int a=nums1[i];
	for(int j=0; j<l2; j++) {
		if(a==nums2[j]) {
			b[index++]=a;
		}
	}
}
System.out.println(Arrays.toString(b));

int freq[]=new int[b.length];
int s=0;
for(int i=0; i<b.length; i++) {
	for(int j=i+1; j<b.length; j++) {
		if(b[i]==b[j]) {
			s++;
			freq[j]=-1;
			break;
		}
	}
}

if(b.length-1==s) {
	int []d=new int[2];
	d[0]=b[0];
	d[1]=b[1];
	System.out.println(Arrays.toString(d));
}


if(b.length-1!=s) {
	int t=b.length-s;
	int res[]=new int[t];
	int e=0;
	 for(int i=0; i<b.length; i++) {
		 if(freq[i]==0) {
			 res[e++]=b[i];
		 }
	 }
	 System.out.println(Arrays.toString(res));
}

}

}
