package thread_demo;

public class Demo1 {

	public static void main(String[] args) {
		
		Thread_child tc= new Thread_child();
		System.out.println(tc.getState());
		tc.start();
		System.out.println(tc.getState());
		System.out.println(tc.getId());
		System.out.println(tc.getName());
		System.out.println(tc.getPriority()); 
		System.out.println(tc.isAlive());
		System.out.println(tc.isDaemon());
		System.out.println(Thread.MIN_PRIORITY);
		System.out.println(Thread.MAX_PRIORITY);
		System.out.println(tc.getState());
		for(int i=1; i<=5; i++) {
			System.out.println("Demo1 "+i);
		}
		System.out.println(tc.getState());
	}

}
