package selflearning;

public class Thread_methods extends Thread{

	public void run() {
		for(int i=1; i<=9; i=i+2) {
			System.out.println(Thread.currentThread().getName()+" "+i);
		}
	}
	public static void main(String[] args) throws InterruptedException {
		Thread_methods t1=new Thread_methods();
		Thread_methods t2=new Thread_methods();
		Thread_methods t3=new Thread_methods();
		t1.start();
		t2.start();
		t3.start();
		
		//t2.join();
		for(int i=2; i<=10; i=i+2) {
			Thread.yield();  //wait panni last a run agum
			
			System.out.println(Thread.currentThread().getName()+" "+i);
		}
		
		
	}

}
