package selflearning;

public class Synchronized_demo extends Thread{

	public synchronized void run() {
		print();
	}
	
	public synchronized void print() {
		for(int i=1; i<=5; i++) {
			System.out.println(i);
		}
	}
	public static void main(String[] args) {
		
		Synchronized_demo t1=new Synchronized_demo();
		Synchronized_demo t2=new Synchronized_demo();
		t1.start();
		t2.start();
		System.out.println("main");
	}

}
