package practiceprograms;

import java.util.Scanner;

public class Array_Right_rotate {

	public static void main(String[] args) {
		int [] arr = new int [] {1, 2, 3, 4, 5}; 
		int size=arr.length;
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter how many times to rotate left");
      int n =sc.nextInt();
       
         for(int i = 0; i < n; i++){  
            
         int last = arr[size-1];  
          for(int j = size-1; j>0; j--){  
              
              arr[j] = arr[j-1];  
          }  
           
         arr[0]=last; 
      }  
      System.out.println(); 
      
      //System.out.println("Array after left rotation: ");  
      for(int i = 0; i< arr.length; i++){  
          System.out.print(arr[i] + " ");  
      }  
	}

}
