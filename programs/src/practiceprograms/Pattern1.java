package practiceprograms;

public class Pattern1 {

	public static void main(String[] args) {
		for(int r=1;r<=5;r++) {
			 for(int s=4;s>=r;s--) {
				 System.out.print(" ");
			 }
			for(int c=1;c<=r;c++) {
				System.out.print("* ");
			}
			System.out.println();
		}

		for(int row=1;row<=4;row++) {
			for(int space=1;space<=row;space++) {
				System.out.print(" ");
				
			}
			for(int star=4;star>=row;star--) {
				System.out.print("* ");
			}
			System.out.println();
		}
	}

}
