package practiceprograms;

public class Pattern2 {

	public static void main(String[] args) {
		for(int r=1;r<=4;r++) {
			for(int space=3;space>=r;space--) {
				System.out.print(" ");
			}
			for(int s1=1;s1<r;s1++) {
				System.out.print("*");
			}
			for(int s2=1;s2<=r;s2++) {
				System.out.print("*");
			}
			System.out.println();
		}

		for(int r=1;r<=4;r++) {
			for(int space=1;space<r;space++) {
				System.out.print(" ");
			}
			for(int star1=3;star1>=r;star1--) {
				System.out.print("*");
			}
			for(int star2=4;star2>=r;star2--) {
				System.out.print("*");
			}
			System.out.println();
		}
		
	}

}
