package practiceprograms;

public class Duplicatearray {

	public static void main(String[] args) {
		int a[]={1,2,4,3,3,1,6,5,6};
		
		
		int[] uniq = new int[a.length];
		for(int i=0;i<a.length;i++){
			int count=1;
			for(int j=i+1;j<a.length;j++){
				if(a[i]==a[j])
				{
					count++;
					uniq[j]=-1;
				}
			}
			if (uniq[i] != -1) {
			uniq[i]=count;
			}
		}		
		
		for (int i = 0; i < a.length; i++) {
			if (uniq[i] > 0) {
				System.out.print(a[i]+" ");

			}
		}
		
		
}
}