package practiceprograms;

import java.util.Arrays;

public class Arrat_Return {

	public static void main(String[] args) {
		int[] a = { 4, 1, 3, 5, 2 };
		int[] b = new int[a.length];

		Arrat_Return obj = new Arrat_Return();

		b = obj.Return_array(a);
		for(int i=0;i<b.length;i++) {
			System.out.print(b[i]+" ");
		}
		System.out.println();
		System.out.println(Arrays.toString(b));

	}

	public int[] Return_array(int[] a) {
		int i;

		for (i = 0; i < a.length; i++) {
			int temp = 0;
			for (int j = i + 1; j < a.length; j++) {
				if (a[i] > a[j]) {
					temp = a[i];
					a[i] = a[j];
					a[j] = temp;

				}
			}
			// System.out.print(a[i]+" ");

		}
		// System.out.println();
		// System.out.println(Arrays.toString(a));
		return a;
	}

}
